# yay, let's make assumptions!

# extract archlinux bootstrap image
/busybox tar -xzf archlinux-bootstrap-x86_64.tar.gz
# the image contains everything inside a subdirectory named "root.x86_64"

# since docker mounts /dev, /proc, /sys and some files inside /etc
# , everything must be moved manually

# move everything from extracted root to root, without overwriting anything
/busybox mv -n root.x86_64/* /
# same for everything inside extracted root's /etc
/busybox mv -n root.x86_64/etc/* /etc/
# /dev, /proc and /sys are empty in the extracted root

# clean up afterwards
/busybox rm -Rf root.x86_64
/busybox rm archlinux-bootstrap-x86_64.tar.gz
/busybox rm busybox
